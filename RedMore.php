<?php require_once 'assets/common/header.php';
$database = new classess\database('root', '', 'blog');
if(!isset($_GET['id']) && empty($_GET['id'])){
    header('location:panel.php');
}
if(isset($_POST['OK'])){
    $sql = 'UPDATE Ad SET status=? WHERE id=?';
    $database->do($sql,array(1,$_GET['id']));
    header('location:panel.php');
}

    $sql = 'SELECT * FROM Ad where id=?';
    $data = $database->select($sql, array($_GET['id']) , 'fetch');
?>
<div class="container">
    <div class="col-sm-12">

        <div class="panel panel-primary">
            <div class="panel-heading"><?php echo $data->title ?></div>
            <div class="panel-body">
                <?php echo $data->content?>
                <hr>
                <h4 class="text-center">اطلاعات درباره آگهی دهنده</h4>
               <center>
                   <?php echo $data->user_about?>

               </center>
            </div>
            <div class="panel-footer">
                <label for="">راه ارتباط :</label>
                <?php
                if(!isset($_SESSION['Login'])){

                    echo '<div class="alert alert-danger">کاربر عزیز برای نمایش راه ارتباطی با آگهی دهنده اول باید ثبت نام کنید</div>';
            }else{
                ?>
                <a style="float: left"><?php echo $data->conect ?></a>

                <form method="post">
                    <?php
                if($_SESSION['emailuser'] == 'mobiniranir@gmail.com'){
                    ?>
                    <button class="btn btn-primary" name="OK">تایید آگهی</button>
                    <?php
                }
                ?>
                    <?php }?>
                </form>

            </div>

        </div>
    </div>
</div>
<?php require_once 'assets/common/footer.php'; ?>
