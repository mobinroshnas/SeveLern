<?php require_once 'assets/common/header.php';
require_once 'classess/database.php';
$database = new  \classess\database('root', '', 'blog');
$message = '';
$messageLogin = '';
if(isset($_SESSION['Login'])){
    header('location:index.php');
}
if (isset($_POST['login'])) {
    $sql = 'SELECT * FROM users WHERE username=? AND password=?';
    $login = $database->select($sql, array($_POST['usernameLogin'], $_POST['passwordLogin']), 'fetch');
    if ($login == true) {
        if(isset($_POST['checkbox'])){
            setcookie('username' , $_POST['usernameLogin'] , time() + (48 * 60 * 60));
            setcookie('password' , $_POST['passwordLogin'] , time() + (48 * 60 * 6));
        }
        if(!isset($_POST['checkbox'])){
            setcookie('username' , $_POST['usernameLogin'] , time() - (48 * 60 * 60));
            setcookie('password' , $_POST['passwordLogin'] , time() - (48 * 60 * 6));
        }
        @$_SESSION['Login'] = 'ok';
        @$_SESSION['user_id'] = $login->id;
        @$_SESSION['user_name'] = $login->name;
        @$_SESSION['emailuser'] = $login->email;
        @$_SESSION['username'] = $login->username;
        @$_SESSION['password'] = $login->password;
        @$_SESSION['img_user'] = $login->img;
        @$_SESSION['rule'] = $login->rule;
        @$_SESSION['about'] = $login->about;
        header('location:index.php');
    } else {
        $messageLogin = 'نام کاربری یا رمزعبور شما اشتباه است';
    }
}
?>

<div class="container">
    <div class="col-lg-8 col-lg-offset-2">
        <div class="panel panel-primary">
            <?php
            if (!empty($messageLogin)) {
                echo '<div class="alert alert-danger">' . $messageLogin . '</div>';
            }
            ?>
            <div class="panel-heading">ورود
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <form class="form-horizontal" method="post">
                            <div class="form-group">
                                <input type="text" name="usernameLogin" class="form-control" value="<?php  if(isset($_COOKIE['username'])){echo $_COOKIE['username'];} ?>" placeholder="نام کاربری" >
                            </div>
                            <div class="form-group">
                                <input type="password" name="passwordLogin" class="form-control" value="<?php  if(isset($_COOKIE['password'])){echo $_COOKIE['password'];}
                                ?>" placeholder="رمز عبور"  >
                            </div>
                            <label class="container1">
                                <input name="checkbox" type="checkbox" <?php if(isset($_COOKIE['username'])){
                                    echo 'checked';
                                } ?>>
                                <span class="checkmark"></span>
                                <span class="textcheckbox">مرا به خاطر بسپار</span>
                            </label>
                            <button type="submit" name="login" class="btn btn-default">ورود</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<?php require_once 'assets/common/footer.php';

?>
