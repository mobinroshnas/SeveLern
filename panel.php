<?php require_once 'assets/common/header.php';
if (!isset($_SESSION['Login'])) {
    header('location:login.php');
}
$massage = '';
$database = new \classess\database('root', '', 'blog');
?>

    <div class="container">
    <div class="col-lg-8 col-lg-offset-2">
    <div class="panel panel-primary">
    <div class="panel-heading">
        <h4>مدیریت آگهی ها</h4>

    </div>
    <div class="panel-body">
    <div class="table-responsive">
    <table class="table">
    <thead>
    <tr>
        <th>نام</th>
        <th>عنوان آگهی</th>
        <th>وضعیت</th>
        <th>دسته بندی</th>
        <th>عملیات</th>
    </tr>
    </thead>
    <tbody>
<?php
if ($_SESSION['emailuser'] == 'mobiniranir@gmail.com') {
    $sql = 'SELECT * FROM Ad';
} else {
    $sql = 'SELECT * FROM Ad WHERE user_id=?';
}
$data = $database->select($sql, array($_SESSION['user_id']), 'fetchall')
?>
<?php
if (!empty($data)) {
    foreach ($data as $value) { ?>
        <tr>
            <td><?php echo $value->user_name ?></td>
            <td><?php echo $value->title ?></td>
            <td>
              <?php
              if($value->status == 0){
                  echo '  <p class="text-danger"> 
                تایید نشده
                </p>';
              }
              if($value->status == 1){
                  echo '  <p class="text-success"> 
                فعال
                </p>';
              }
              ?>
            </td>
            <td>
                <?php
                if($value->category == 1){
                    echo 'برنامه نویسی';
                }
                if($value->category == 2){
                    echo 'گرافیک';
                }
                if($value->category == 3){
                    echo 'الکترونیک';
                }
                if($value->category == 4){
                    echo 'آی تی';
                }
                ?>
            </td>
            <td>
                <a class="btn btn-warning" href="RedPanel.php?id=<?php echo $value->id ?>">مشاهده اگهی</a>
            </td>
        </tr>
    <?php }}else{
    echo '<div class="alert alert-warning">هیچ آگهی ثبت نشده است</div>';
} ?>
    </tbody>
    </table>
    </div>
    </div>
    <div class="panel-footer ">
        <a href="new.php">
            <button class="btn btn-success btn-block">ایجاد آگهی</button>
        </a>
    </div>
    </div>
    </div>
    </div>
    <?php require_once 'assets/common/footer.php'; ?>