<?php require_once 'assets/common/header.php';
$massage = '';
$database = new \classess\database('root', '', 'blog');
if (!isset($_SESSION['Login'])) {
    header('location:login.php');
}
if (!isset($_GET['user_id']) && empty($_GET['user_id'])) {
    header('location:profile.php');

}
 ?>

<?php
if (isset($_POST['Edit'])) {
    $filename = $_FILES['file']['name'];
    $tmpname = $_FILES['file']['tmp_name'];
    $level1 = explode('.', $filename);
    $passvand = end($level1);
    if (in_array($passvand, array('png', 'jpg', 'jpeg', ''))) {
        if ($passvand == '') {
            $name = 'تصویری وجود ندارد';
        } else {
            $name = rand(0, 1000) . '__' . rand(1, 800) . '.' . $passvand;
            move_uploaded_file($tmpname, 'assets/img/' . $name);
        }
    } else {
        $massageError = 'پسوند شما مجاز نمیباشد';
    }
    $sql = 'UPDATE users SET name=? , username=?,rule=? , about=? , img=? WHERE id=?';
    if (!isset($_FILES['file'])) {
        $name = 'not pic';
    }
    $edit = $database->do($sql, array($_POST['nameEdit'], $_POST['usernameEdit'], $_POST['rule'], $_POST['editor1'], $name, $_GET['user_id']));
    if($edit == true){
        header('location:profile.php');
    }
}
?>


    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4>ویرایش پروفایل</h4>
                </div>
                <div class="panel-body">
                    <?php
                    if (!empty($massage)) {
                        echo '<div class="alert alert-warning">' . $massage . '</div>';
                    }
                    ?>
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="">نام و نام خوانوادگی</label>
                            <input type="text" name="nameEdit" class="form-control"
                                   value="<?php echo $_SESSION['user_name'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="">نام کاربری</label>
                            <input type="text" name="usernameEdit" class="form-control"
                                   value="<?php echo $_SESSION['username'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="">تخصص</label>
                            <input type="text" name="rule" class="form-control"
                                   value="<?php echo $_SESSION['rule'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="">درباره من</label>
                           <textarea name="editor1" rows="5"><?php echo $_SESSION['about']?></textarea>
                        </div>
                        <span class="btn btn-primary btn-file">
                               آپلود فایل <input name="file" type="file">
                           </span>
                        <button type="submit" name="Edit" class="btn btn-danger">ویرایش</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
<?php require_once 'assets/common/footer.php'; ?>