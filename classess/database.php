<?php
namespace classess;

class database
{
    public $pdo;
    private $username;
    private $password;
    private $dbname;
    private $option;

    public function __construct($username, $password, $dbname)
    {
        $this->username = $username;
        $this->password = $password;
        $this->dbname = $dbname;
        $this->option = [
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_WARNING
        ];
        $this->pdo = new \PDO('mysql:host=localhost;dbname=' . $this->dbname . ';', $this->username, $this->password, $this->option);
        return $this->pdo;
    }

    public function do($sql, $value = [], $type = 'insert')
    {
        $result = $this->pdo->prepare($sql);
        foreach ($value as $key => $data) {
            $result->bindValue($key + 1, $data);
        }
        if ($type == 'insert') {
            return $result->execute();
        } else {
            $result->execute();
            if ($result->rowCount() >= 1) {
                return $result;
            } else {
                return false;
            }
        }
    }

    public function select($sql , $value = [] , $type = 'fetchall'){
        $result = $this->pdo->prepare($sql);
        foreach ($value as $key => $data){
            $result->bindValue($key +1, $data);
        }
        $result->execute();
        if($type == 'fetchall'){
        if($result->rowCount() >= 1){
            $row = $result->fetchAll(\PDO::FETCH_OBJ);
            return $row;
        }else{
            return false;
        }
        }else{
            if($result->rowCount() >= 1){
                $row = $result->fetch(\PDO::FETCH_OBJ);
                return $row;
            }else{
                return false;
            }
        }
    }
}
