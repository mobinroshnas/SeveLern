<?php require_once 'assets/common/header.php';
require_once 'autoload.php';
$database = new classess\database('root', '', 'blog');
$massage = '';
if (isset($_POST['send'])) {
    $sql = 'INSERT INTO Ad SET title=? , content=? , category=? , user_id=? , user_name=? , user_about=? , conect=?';
    $ads = $database->do($sql, array($_POST['title'], $_POST['editor1'], $_POST['category'], $_SESSION['user_id'], $_SESSION['user_name'] , $_SESSION['about'],$_POST['conect']));
    if ($ads == true) {
        $massage = 'آگهی شمابا موفقیت برای تایید به مدیر ارسال شد';
    }else{
        $massage = 'تیکیت شما با موفقیت ثبت نشد دوباره امتحان کنید';
    }
}
?>
<div class="container">
    <div class="col-sm-12">

        <div class="panel panel-primary">
            <div class="panel-heading">ارسال آگهی جدید</div>
            <div class="panel-body">
                <?php
                if (!empty($massage)) {
                    echo '<div class="alert alert-success">' . $massage . '</div>';
                }
                ?>
                <form class="form-horizontal" method="post">
                    <div class="form-group " style="
    margin: 10px;">
                        <label for="">عنوان اگهی</label>
                        <input type="text" name="title" class="form-control"
                               placeholder="عنوان آگهی">
                    </div>
                    <div class="form-group " style="
    margin: 10px;">
                        <p for=" ">دسته بندی:</p>
                        <select class="form-control" name="category" id="sel1">
                            <option value="1">برنامه نویسی</option>
                            <option value="2">گرافیک</option>
                            <option value="3">الکترونیک</option>
                            <option value="4">آی تی</option>
                        </select>
                    </div>
                    <div class="form-group " style="
    margin: 10px;">
                        <label>پیام:</label>
                        <textarea name="editor1" class="form-control" rows="5" id="comment"></textarea>
                    </div>
                    <div class="form-group " style="
    margin: 10px;">
                        <input type="text" name="conect" class="form-control"
                               placeholder="ایدی تلگرام یا شماره تلفن همراه">
                    </div>
                    <button class="btn btn-success btn-block" name="send">ارسال آگهی</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php require_once 'assets/common/footer.php'; ?>
