<?php require_once 'assets/common/header.php';
if(!isset($_GET['id']) && empty($_GET['id'])) {
    header('location:panel.php');
}
if(isset($_POST['delete'])){
    $sql = 'DELETE FROM articles WHERE id=?';
    $database->do($sql , array($_GET['id']));
    header('location:panel.php');
}
?>
<div class="welcome" style="margin-top: -20px">
    <div class="container">
        <div class="jumbotron" style="background-color: #bfe8ff;height: 385px;">
            <h1 class="display-4">خوش آمدید!</h1>
            <p class="lead"> اینجا یک وبلاگ خیلی ساده س که درمورد حوزه ی تکنولوژی و برنامه نویسی هستش .</p>
            <hr class="my-4">
            <p>اگه میخوای بیشتر از ما بدونی روی دکمه زیر بزن و وارد شبکه های اجتماعی ما شو.</p>
            <a class="btn btn-primary" href="#">کلیک کنید</a>
        </div>
    </div>
</div>
<div class="album py-5 bg-light">
    <div class="container">
        <div class="row">
            <?php
            $sql = 'SELECT * FROM articles';
            $select = $database->select($sql );
            if(!empty($select)){
                foreach ($select as $data){

                    ?>
                    <div class="col-md-10 offset-1">
                        <div class="card mb-12 shadow-sm">
                            <img src="assets/img/<?php echo $data->file ?>" width="100%" height="400px" alt="">
                            <div class="card-title text-center">
                                <h5><?php echo $data->title ?><h5>
                            </div>
                            <div class="card-body">
                                <p class="card-text text-center"><?php echo $data->body ?></p>
                                <?php
                                if($_SESSION['emailuser'] == 'mobiniranir@gmail.com'){


                                ?>
                                    <form method="post">
                                <button class="btn btn-danger" name="delete"> حذف پست</button>
                                    </form>
                                    <?php }?>
                            </div>
                        </div>
                    </div>
                <?php }}?>
        </div>
    </div>
</div>

<?php require_once 'assets/common/footer.php'?>
