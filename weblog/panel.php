<?php require_once 'assets/common/header.php';
if (!isset($_SESSION['Login'])) {
    header('location:login.php');
}
$massage = '';
$database = new \classess\database('root', '', 'blog');

?>

    <div class="container">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4>مدیریت آگهی ها</h4>

                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>نویسنده</th>
                                <th>عنوان پست</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                $sql = 'SELECT * FROM articles';

                            $data = $database->select($sql)
                            ?>
                            <?php
                            if (!empty($data)) {
                                foreach ($data as $value) { ?>
                                    <tr>
                                        <td><?php echo $value->user_name ?></td>
                                        <td><?php echo $value->title ?></td>

                                        <td>
                                            <a class="btn btn-warning" href="redmore.php?id=<?php echo $value->id ?>">مشاهده پست</a>
                                        </td>
                                    </tr>
                                <?php }}else{
                                echo '<div class="alert alert-warning">هیچ پستی ثبت نشده است</div>';
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="panel-footer ">
                    <a href="new.php">
                        <button class="btn btn-success btn-block">ایجاد آگهی</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php require_once 'assets/common/footer.php'; ?>