<?php require_once 'assets/common/header.php'?>
        <div class="welcome" style="margin-top: -20px">
            <div class="container">
                <div class="jumbotron" style="background-color: #bfe8ff;height: 385px;">
                    <h1 class="display-4">خوش آمدید!</h1>
                    <p class="lead"> اینجا یک وبلاگ خیلی ساده س که درمورد حوزه ی تکنولوژی و برنامه نویسی هستش .</p>
                    <hr class="my-4">
                    <p>اگه میخوای بیشتر از ما بدونی روی دکمه زیر بزن و وارد شبکه های اجتماعی ما شو.</p>
                    <a class="btn btn-primary" href="#">کلیک کنید</a>
                </div>
            </div>
        </div>
<div class="album py-5 bg-light">
    <div class="container">
        <h2 class="text-center">آخرین مطالب</h2>
        <div class="row">
            <?php
            $sql = 'SELECT * FROM articles';
            $select = $database->select($sql );
            if(!empty($select)){
                foreach ($select as $data){

            ?>
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <img src="assets/img/<?php echo $data->file ?>" width="100%" height="250px" alt="">
                    <div class="card-title text-center">
                        <h5><?php echo $data->title ?><h5>
                    </div>
                    <div class="card-body">
                        <p class="card-text text-center"><?php echo $data->body ?></p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <a href="redmore.php?id=<?php echo $data->id ?>">
                                    <button type="button" class="btn btn-sm btn-outline-secondary">ادامه مطلب</button>
                                </a>
                            </div>
                            <small class="text-muted"><?php echo $data->user_name ?></small>
                        </div>
                    </div>
                </div>
            </div>
<?php }}?>
        </div>
    </div>
</div>

<?php require_once 'assets/common/footer.php'?>
