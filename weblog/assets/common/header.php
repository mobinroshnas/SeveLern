<?php
session_start();
require_once __DIR__ . DIRECTORY_SEPARATOR . '../../../autoload.php';
$database = new classess\database('root', '', 'blog');

?>
<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mobin.ir</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-rtl.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="assets/ckeditor/ckeditor.js"></script>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light sticky" id="main-navbar" data-toggle="sticky-onscroll"
     style="border:0;padding-top: 1.3rem; background-color: white; padding-bottom: 1.3rem; transition: all 0.6s ease 0s; box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 15px;">
    <a href="index.php" class="navbar-logo"><img src="assets/img/download.png" width="150" height="80"></a>
    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
        <li class="active">
            <a class="nav-link" style="color:blue;" href="#">

                <?php
                if (!isset($_SESSION['Login'])) {
                    echo '<a href="About.php">  فلان چیست؟</a>';
                } else {
                    echo '<a href="blog/../new.php">ایجاد آگهی جدید</a>';
                }
                ?>
                <span class="sr-only">(current)</span></a>
        </li>

    </ul>

    <nav class="navbar">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item left">
                <?php
                if (!isset($_SESSION['Login'])) {
                    ?>
                    <a href="blog/../register.php">
                        <button class="btn btn-primary">ثبت نام</button>
                    </a>
                    <a href="blog/../login.php">
                        <button class="btn btn-warning">ورود</button>
                    </a>
                <?php } else {
                    echo '<a href="blog/../AllUsers.php">  <button class="btn btn-dark"> کل کاربران</button></a>';

                    echo '<a href="../profile.php">  <button class="btn btn-primary"> پروفایل من</button></a>';
                    echo '<a href="../panel.php">  <button class="btn btn-warning"> آگهی</button></a>';
                    echo '<a href="../loguot.php">  <button class="btn btn-danger"> خروج</button></a>';

                }
                ?>
            </li>
        </ul>
    </nav>
</nav>
<div class="menu-bottom">
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="border: 0;    border-radius: 0;">
        <div class="container">
            <a class="navbar-brand" href="#">فلان</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../index.php">خانه <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../index.php">وبلاگ </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../About.php">درباره ما</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">تماس با ما</a>
                    </li>
                    <?php
                    if (isset($_SESSION['Login'])) {
                        if ($_SESSION['emailuser'] == 'mobiniranir@gmail.com') {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link" href="blog/../panel.php">پنل وبلاگ</a>
                            </li>
                        <?php }
                    } ?>
                </ul>

            </div>
    </nav>
</div>



