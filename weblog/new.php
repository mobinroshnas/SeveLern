<?php require_once 'assets/common/header.php';
$massage = '';
$massageError = '';
$database = new \classess\database('root', '', 'blog');
?>
<?php
if (isset($_POST['send'])) {
    $filename = $_FILES['file']['name'];
    $tmpname = $_FILES['file']['tmp_name'];
    $level1 = explode('.', $filename);
    $passvand = end($level1);
    if (in_array($passvand, array('png', 'jpg', 'jpeg', ''))) {
        if ($passvand == '') {
            $name = 'تصویری وجود ندارد';
        } else {
            $name = rand(0, 1000) . '__' . rand(1, 800) . '.' . $passvand;
            move_uploaded_file($tmpname, 'assets/img/' . $name);
        }
    } else {
        $massageError = 'پسوند شما مجاز نمیباشد';
    }
    $sql = 'INSERT INTO articles SET title=?,body=?,file=?,user_name=? , user_id=?';
    if (!isset($_FILES['file'])) {
        $name = 'not pic';
    }
    $send = $database->do($sql, array($_POST['title'], $_POST['editor1'], $name, $_SESSION['user_name'], $_SESSION['user_id']));
    if ($send == true) {
        $massage = 'پست شما با موفقیت ارسال شد';
    } else {
        $massage = 'در ارسال پست به مشکل برخوردیم';
    }
}
?>
<div class="container">
    <div class="col-sm-12">

        <div class="panel panel-primary">
            <div class="panel-heading">ارسال پست جدید</div>
            <div class="panel-body">
                <?php
                if (!empty($massage)) {
                    echo '<div class="alert alert-success">' . $massage . '</div>';
                }
                ?>
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-group " style="
    margin: 10px;">
                        <label for="">عنوان پست</label>
                        <input type="text" name="title" class="form-control"
                               placeholder="عنوان پست">
                    </div>

                    <div class="form-group " style="
    margin: 10px;">
                        <label>متن:</label>
                        <textarea name="editor1" class="form-control" rows="5" id="comment"></textarea>
                    </div>

                    <span class="btn btn-primary btn-file">
                               آپلود فایل <input name="file" type="file">
                           </span>
                    <button class="btn btn-success btn-block" name="send">ارسال پست</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php require_once '../assets/common/footer.php'; ?>
