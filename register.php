<?php require_once 'assets/common/header.php';
require_once 'classess/database.php';
$database = new  \classess\database('root', '', 'blog');
$message = '';
$messageERROR = '';
if (isset($_SESSION['Login'])) {
    header('location:index.php');
}
if (isset($_POST['reg'])) {
    $sql = 'SELECT * FROM users WHERE email=?';
    $con = $database->select($sql, array($_POST['email']), 'fetch');
    if ($con == true) {
        $messageERROR = 'این ایمیل  از قبل وجود دارد';
    } else {
        $sql = 'INSERT INTO users SET name=? , username=? , email=? , password=?';
        $data = $database->do($sql, array($_POST['name'], $_POST['username'], $_POST['email'], $_POST['password']), 'insert');
        if ($data == true) {
            header('location:login.php');
        }
    }
}
?>

<div class="container">
    <div class="col-lg-8 col-lg-offset-2">
        <div class="panel panel-primary">
            <div class="panel-heading">ثبت نام
            </div>
            <div class="panel-body">
                <?php
                if (!empty($messageERROR)) {
                    echo '<div class="alert alert-warning">' . $messageERROR . '</div>';
                }
                ?>
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <form class="form-horizontal" method="post">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control"
                                       placeholder="(فارسی)نام و نام خوانوادگی">
                            </div>
                            <div class="form-group">
                                <input type="text" name="username" class="form-control" placeholder="نام کاربری">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="ایمیل">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="رمز عبور">
                            </div>

                            <button type="submit" name="reg" class="btn btn-default">ثبت نام</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<?php require_once 'assets/common/footer.php';

?>
