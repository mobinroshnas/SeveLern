<?php require_once 'assets/common/header.php'; ?>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            دباره فلان
        </div>
        <div class="panel-body">
            <p class="text-center text-justify">
                فلان یک وب سایت آگهی هست ولی با سایت های آگهی دیگه فرق میکنه اون فرق هم اینه که تمرکز اصلی روی کسایی هستش که سرمایه گزارن ودنبال ایده هستن یا ایده پردازن و دنبال سرمایه هستن
                <br>
                <br>
                وب سایت فلان کاملا رایگان هستش و شما رایگان میتونید آگهی خودتون رو رایگان میتونید در سایت قرار بدید.
                <br>
                <br>
                اما اگه مایل به حمایت از ما هستید میتونید از لینک زیر با ارز های دیجیتال بیت کوین از ما حمات کنید
                <br>
                <br>
                <a href="#">
                    <button class="btn btn-primary">حمایت از ما</button>
                </a>
            </p>
        </div>
    </div>
</div>
<?php require_once 'assets/common/footer.php'?>