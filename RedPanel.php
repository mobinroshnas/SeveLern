<?php require_once 'assets/common/header.php';
$database = new classess\database('root', '', 'blog');
if(!isset($_GET['id']) && empty($_GET['id'])){
    header('location:panel.php');
}


$sql = 'SELECT * FROM Ad where id=?';
$data = $database->select($sql, array($_GET['id']) , 'fetch');
if(isset($_POST['delete'])){
    $sql = 'DELETE FROM Ad WHERE id=?';
    $database->do($sql , array($_GET['id']));
    header('location:panel.php');
}
?>
<div class="container">
    <div class="col-sm-12">

        <div class="panel panel-primary">
            <div class="panel-heading"><?php echo $data->title ?></div>
            <div class="panel-body">
                <?php echo $data->content?>
            </div>
            <div class="panel-footer">
                <label for="">راه ارتباط :</label>
                <a style="float: left"><?php echo $data->conect ?></a>
                <form method="post">

                        <button class="btn btn-danger" name="delete">حذف آگهی</button>


                </form>

            </div>

        </div>
    </div>
</div>
<?php require_once 'assets/common/footer.php'; ?>
